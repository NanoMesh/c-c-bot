﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_and_Conquer_Bot
{
    public class Settings
    {

        public string BotUserName { get; set; }
        public string BotLoginToken { get; set; }
        public ulong WelcomeChannelId { get; set; }
        public ulong ModChannelId { get; set; }
        public ulong ModRoleId { get; set; }
        public ulong BattlefieldChannel { get; set; }
        public List<ulong> RandomCrateChannels { get; set; }
        public int HarvesterDelay { get; set; }

        public Settings()
        {
            BotUserName = "Default Bot Name";
            BotLoginToken = "Default Bot Token";
            WelcomeChannelId = 1234;
            ModChannelId = 1324;
            ModRoleId = 13221;
            BattlefieldChannel = 123;
            RandomCrateChannels = new List<ulong>();
            HarvesterDelay = 9;
        }

    }
}
