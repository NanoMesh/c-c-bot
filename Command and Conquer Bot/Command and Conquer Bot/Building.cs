﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_and_Conquer_Bot
{
    public enum BuildingTypeEnum
    {
        PowerPlant,
        Refinery,
        Barracks,
        Factory,
        Airfield
    }

    public class Building
    {
        public BuildingTypeEnum BuildingType { get; set; }
        public string BuildingName { get; set; }
        public int BuildingCost { get; set; }
        public string BuildingImagePath { get; set; }
        public int BuildingCount { get; set; }
        public int BuildingPowerCost { get; set; }

        public Building()
        {
            BuildingType = BuildingTypeEnum.PowerPlant;
            BuildingName = "Default Building Name";
            BuildingCost = 1000;
            BuildingImagePath = "Default Image Path";
            BuildingCount = 0;
            BuildingPowerCost = 10;
        }
    }
}
