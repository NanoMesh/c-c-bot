﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_and_Conquer_Bot
{
    public enum UnitTypeEnum
    {
        Harvester,
        Infantry,
        Vehicle,
        Aircraft
    }
    public class Unit
    {
        public string UnitName { get; set; }
        public int UnitCost { get; set; }
        public int UnitCount { get; set; }
        public int UnitDmg { get; set; }
        public UnitTypeEnum UnitType { get; set; }
        public string UnitImagePath { get; set; }

        public Unit()
        {
            UnitName = "Default Unit Name";
            UnitCost = 100;
            UnitCount = 0;
            UnitDmg = 5;
            UnitType = UnitTypeEnum.Infantry;
            UnitImagePath = "Default Image Path";
        }
    }
}
