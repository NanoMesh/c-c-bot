﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Discord;
using Newtonsoft.Json;

namespace Command_and_Conquer_Bot
{
    public class BasicTasks
    {
        //
        //Constructor for this class with reference to the main Core class for accesing its public methods.
        //
        public Core BotCore;
        public BasicTasks(Core cor)
        {
            BotCore = cor;
        }

        //┌────────────────────────────────────────────────────────────────────────────────────┐
        //│ Loads Settings object from Data\\Settings.json, if file not found than creates new │
        //└────────────────────────────────────────────────────────────────────────────────────┘
        public async Task<Settings> LoadSettingsTask()
        {
            Settings tempSet = new Settings();
            try
            {

            BotCore.PrintUsingColor(ConsoleColor.DarkGreen, "[O_O]", "LOADING SETTINGS");
            //
            //Check if Folder structure exists
            //
            if (!Directory.Exists("Data")) { Directory.CreateDirectory("Data"); }
            if (!Directory.Exists("Logs")) { Directory.CreateDirectory("Logs"); }


            //
            //Tries to load data from Settings.json file
            //
            if (File.Exists("Data\\Settings.json"))
            {
            string jason = File.ReadAllText("Data\\Settings.json");
            tempSet = JsonConvert.DeserializeObject<Settings>(jason);
            BotCore.PrintUsingColor(ConsoleColor.DarkGreen, "[O_O]", "SETTINGS SUCCESSFULLY LOADED");
            }
            //
            //Settings.json file not found, create new one with deffault settings.
            //
            else
            {
                BotCore.PrintUsingColor(ConsoleColor.DarkGreen, "[O_O]", "SETTINGS FILE NOT FOUND, CREATING");
                string jason = JsonConvert.SerializeObject(tempSet);
                File.WriteAllText("Data\\Settings.json", jason);
            }

            
            await Task.Delay(100);
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Loading Task {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
            return tempSet;
        }
        //┌──────────────────────────┐
        //│ Loads Factions and Users │
        //└──────────────────────────┘
        public async Task LoadFactUsersTask()
        {
            try
            {

            if (File.Exists("Data\\Commanders.json"))
            {
                string jason = File.ReadAllText("Data\\Commanders.json");
                if (jason != null || jason != "")
                {
                BotCore.DictionaryOfCommanders = JsonConvert.DeserializeObject<Dictionary<ulong, Commander>>(jason);
                }

            }
            if (File.Exists("Data\\Factions.json"))
            {
                string jason = File.ReadAllText("Data\\Factions.json");
                BotCore.ListOfFactions = JsonConvert.DeserializeObject<List<Faction>>(jason);
            }
            if (File.Exists("Data\\Crates.json"))
            {
                string jason = File.ReadAllText("Data\\Crates.json");
                BotCore.ListOfCrates = JsonConvert.DeserializeObject<List<Crate>>(jason);
                foreach(Crate cr in BotCore.ListOfCrates)
                {
                    cr.CrateLastTimeSent = DateTime.Now;
                    Random rand = new Random();
                    cr.CrateDelay = rand.Next(cr.CrateDelayThresholdLow, cr.CrateDelayThresholdHigh);
                    BotCore.PrintUsingColor(ConsoleColor.DarkRed, "Crate", $"Set Crate delay to {cr.CrateDelay}");
                }
            }
            await Task.Delay(100);
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Loading Factions Task {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌──────────────────────────────────────────────────────────────────┐
        //│ Sets a Random Game from a list fo strings in Data\\GameList.json │
        //└──────────────────────────────────────────────────────────────────┘
        public async Task<Game> SetRandomGameTask()
        {
            Game gam = new Game("Bot Made by NanoMesh");
            try
            {

            //
            //If gamelist file exists
            //
            if (File.Exists("Data\\GameList.json"))
            {
                List<string> gamelist = new List<string>();
                Random rand = new Random();
                string jason = File.ReadAllText("Data\\GameList.json");
                gamelist = JsonConvert.DeserializeObject<List<string>>(jason);
                gam = new Game(gamelist[rand.Next(gamelist.Count)]);

            }
            //
            //If it doesn't exist, create one
            //
            else
            {
                List<string> gamelist = new List<string>();
                gamelist.Add("Example Game #1");
                gamelist.Add("Example Game #2");
                string jason = JsonConvert.SerializeObject(gamelist);
                File.WriteAllText("Data\\GameList.json", jason);
            }

            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in RandomGameTask {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
            await Task.Delay(100);
            return gam;
        }
        //┌────────────────────────────────────────────┐
        //│ Saves Chatlog, Users and Factions to files │
        //└────────────────────────────────────────────┘
        public async Task SaveDataTask()
        {
            try
            {

            BotCore.PrintUsingColor(ConsoleColor.Red, "[O_O]", "SAVING DATA");
            var tempList = new List<string>();
            tempList.AddRange(BotCore.BotLogMessageList);
            BotCore.BotLogMessageList.Clear();
            //
            //Save Log
            //
            File.AppendAllLines($"Logs\\{DateTime.Now.Day.ToString()} {DateTime.Now.Month.ToString()} {DateTime.Now.Year.ToString()} Chat Log.txt", tempList);
            await Task.Delay(100);
            //
            //Save Commanders
            //
            var jason = JsonConvert.SerializeObject(BotCore.DictionaryOfCommanders);
            File.WriteAllText("Data\\Commanders.json", jason);
            //
            //Save Factions
            //
            jason = JsonConvert.SerializeObject(BotCore.ListOfFactions);
            File.WriteAllText("Data\\Factions.json", jason);
            BotCore.PrintUsingColor(ConsoleColor.Green, "[O_O]", "SAVING DATA COMPLETE");
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Save Log Task {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌──────────────────────────┐
        //│ Add role to a IGuildUser │
        //└──────────────────────────┘
        public async Task AddRoleTask(IGuildUser usr, string roleName)
        {
            try
            {
                bool canjoin = true;
                foreach (IRole rl in usr.Roles)
                {
                    if (rl.Name.Contains(roleName))
                    {
                        canjoin = false;
                    }
                }
                if (canjoin)
                {

                    foreach (IRole rl in usr.Guild.Roles)
                    {
                        if (rl.Name.Contains(roleName))
                            {
                            await usr.AddRolesAsync(rl);
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Adding Role {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌────────────────────────────────┐
        //│ Removes role from a IGuildUser │
        //└────────────────────────────────┘
        public async Task RemoveRoleTask(IGuildUser usr, string roleName)
        {
            try
            {
                bool canLeave = false;
                foreach (IRole rl in usr.Roles)
                {
                    if (rl.Name.Contains(roleName))
                    {
                        canLeave = true;
                    }
                }
                foreach (IRole rl in usr.Guild.Roles)
                {
                    if (rl.Name.Contains(roleName))
                    {
                        await usr.RemoveRolesAsync(rl);
                    }
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Removing Role {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌───────────────────────────────┐
        //│ Resets daily based activities │
        //└───────────────────────────────┘
        public async Task DailyResetTask()
        {
            try
            {

            //
            //Reset build limit
            //
            foreach(KeyValuePair<ulong,Commander> cmd in BotCore.DictionaryOfCommanders)
            {
                cmd.Value.BuildLimit = 10;
                cmd.Value.UnitLimit = 20;
            }
            //
            //Harvesters
            //
            foreach(Faction fac in BotCore.ListOfFactions)
            {
                foreach(KeyValuePair<string,Unit> unit in fac.FactionUnits)
                {
                    if(unit.Value.UnitType == UnitTypeEnum.Harvester)
                    {
                        int harvestYield = 2000 * unit.Value.UnitCount;
                        fac.FactionCredits += harvestYield;
                        IMessageChannel chn = await BotCore.BotClient.GetChannelAsync(fac.FactionChannelId) as IMessageChannel;
                            if(chn != null)
                            {
                            await chn.SendMessageAsync($"***Harvesters returned with {harvestYield} credits and build limits have been restored.***");
                            }
                        }

                }

            }

            await Task.Delay(100);
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Daily Reset Task {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
    }
}
