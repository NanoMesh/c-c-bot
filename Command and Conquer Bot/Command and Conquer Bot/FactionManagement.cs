﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using System.IO;

namespace Command_and_Conquer_Bot
{
    public class FactionManagement
    {
        public Core BotCore;

        public FactionManagement(Core core)
        {
            BotCore = core;
        }

        //┌──────────────────────────┐
        //│ Adds user to the Faction │
        //└──────────────────────────┘
        public async Task JoinFactionTask(IMessage message)
        {
            try
            {

            if (BotCore.DictionaryOfCommanders[message.Author.Id].HasJoinedFaction == false)
            {
                string parseFacName = message.Content.ToLower().Substring(message.Content.IndexOf(' ') + 1);
                Faction fac = BotCore.ListOfFactions.Find(x => x.FactionRoleName.ToLower() == parseFacName);
                if(fac != null)
                {

                    BotCore.DictionaryOfCommanders[message.Author.Id].CommanderFactionName = fac.FactionRoleName;
                    BotCore.DictionaryOfCommanders[message.Author.Id].HasJoinedFaction = true;
                    fac.FactionMembers++;
                    await BotCore.BotBasicTasks.AddRoleTask(message.Author as IGuildUser, fac.FactionRoleName);
                    if(fac.FactionRoleName == "NOD")
                    {
                        await message.Channel.SendMessageAsync($"***Commander {message.Author.Username}*** has joined the Brotherhood of Nod");
                    }
                    if(fac.FactionRoleName == "GDI")
                    {
                        await message.Channel.SendMessageAsync($"***Commander {message.Author.Username}*** has joined the Global Defense Initiative");
                    }
                    if(fac.FactionRoleName == "Allies")
                    {
                        await message.Channel.SendMessageAsync($"***Commander {message.Author.Username}*** has joined the Allied forces");
                    }
                    if(fac.FactionRoleName == "Soviets")
                    {
                        await message.Channel.SendMessageAsync($"***Commander {message.Author.Username}*** has joined the Soviet nation");
                    }
                }
            }
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Joining Faction {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌───────────────────────────┐
        //│ Removes user from Faction │
        //└───────────────────────────┘
        public async Task LeaveFactionTask(IMessage message)
        {
            try
            { 
            if (BotCore.DictionaryOfCommanders[message.Author.Id].HasJoinedFaction == true)
            {
                Faction fac = BotCore.ListOfFactions.Find(x => x.FactionRoleName == BotCore.DictionaryOfCommanders[message.Author.Id].CommanderFactionName);
                await BotCore.BotBasicTasks.RemoveRoleTask(message.Author as IGuildUser, fac.FactionRoleName);
                BotCore.DictionaryOfCommanders[message.Author.Id].CommanderFactionName = "none";
                BotCore.DictionaryOfCommanders[message.Author.Id].HasJoinedFaction = false;
                fac.FactionMembers--;
                if (fac.FactionRoleName == "NOD")
                {
                    await message.Channel.SendMessageAsync($"***Commander {message.Author.Username}*** has left the Brotherhood of Nod");
                }
                if (fac.FactionRoleName == "GDI")
                {
                    await message.Channel.SendMessageAsync($"***Commander {message.Author.Username}*** has left the Global Defense Initiative");
                }
                if (fac.FactionRoleName == "Allies")
                {
                    await message.Channel.SendMessageAsync($"***Commander {message.Author.Username}*** has left the Allied forces");
                }
                if (fac.FactionRoleName == "Soviets")
                {
                    await message.Channel.SendMessageAsync($"***Commander {message.Author.Username}*** has left the Soviet nation");
                }
            } }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Leaving Faction {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌───────────────────────────┐
        //│ Displays ordered factions │
        //└───────────────────────────┘
        public async Task TopFactions(IMessage message)
        {
            try
            {

            string topfac = "";
            var lisofFace = BotCore.ListOfFactions.OrderByDescending(x => x.FactionCredits);
            foreach (Faction fac in lisofFace)
            {
                topfac += $"***{fac.FactionName}*** {Environment.NewLine} :busts_in_silhouette: Commanders: {fac.FactionMembers} | :moneybag: Funds: {fac.FactionCredits} | :triangular_flag_on_post: Wins: {fac.NumberOfWins}{Environment.NewLine}";
            }
            await message.Channel.SendMessageAsync(topfac);
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in TopFactionTask {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌─────────────────────┐
        //│ Constructs building │
        //└─────────────────────┘
        public async Task ConstructBuilding(IMessage message)
        {
            try
            {
                // -build X buildingname
                var begginingOfAmount = message.Content.IndexOf(' ', 0);
                var endOfAmount = message.Content.IndexOf(' ', begginingOfAmount + 1);
                int amountToBuild;
                string amountString = message.Content.ToLower().Substring(begginingOfAmount + 1, endOfAmount - begginingOfAmount - 1);
                string ObjToConstruct = message.Content.ToLower().Substring(endOfAmount + 1);


       
                    

                




            //Finds users faction
            Faction fac = BotCore.ListOfFactions.Find(x => x.FactionRoleName == BotCore.DictionaryOfCommanders[message.Author.Id].CommanderFactionName);
            //find if object is in dictionary of that faction
            Building build = new Building();
            //
            //Check for amount
            //
            if (int.TryParse(amountString, out amountToBuild))
            {

                    //
                    //Check if building exists
                    //
                    if (fac.FactionBuildings.TryGetValue(ObjToConstruct, out build))
                    {
                        //
                        //Check if user can build
                        //
                        if (BotCore.DictionaryOfCommanders[message.Author.Id].BuildLimit > 0 && amountToBuild > 0 && amountToBuild <= BotCore.DictionaryOfCommanders[message.Author.Id].BuildLimit)
                        {
                            //
                            //Check if faction has enough credits
                            //
                            if (fac.FactionCredits - (build.BuildingCost*amountToBuild) >= 0)
                            {
                                //
                                //Check for Faction Power Level
                                //
                                if (fac.FactionPowerLevel - (build.BuildingPowerCost * amountToBuild) >= 0)
                                {
                                    string flavourText = "";
                                    //
                                    //Building is power plant
                                    //
                                    if (build.BuildingType == BuildingTypeEnum.PowerPlant)
                                    {
                                        fac.FactionPowerLevel += 100*amountToBuild;
                                        flavourText = $"Power level increased::zap: {fac.FactionPowerLevel}";
                                    }
                                    //
                                    //Building is Barracks, increase infantry limit
                                    //
                                    if (build.BuildingType == BuildingTypeEnum.Barracks)
                                    {
                                        fac.InfantryLimit += 20 * amountToBuild;
                                        flavourText = $"Infantry limit increased::gun: {fac.InfantryLimit}";
                                    }
                                    //
                                    //Building is Airfield, increase aircraft limit
                                    //
                                    if (build.BuildingType == BuildingTypeEnum.Airfield)
                                    {
                                        fac.AircraftLimit += 5 * amountToBuild;
                                        flavourText = $"Aircraft limit increased::airplane: {fac.AircraftLimit}";
                                    }
                                    //
                                    //Building is factory, increase vehicle limit
                                    //
                                    if (build.BuildingType == BuildingTypeEnum.Factory)
                                    {
                                        fac.VehicleLimit += 10 * amountToBuild;
                                        flavourText = $"Vehicle limit increased::red_car: {fac.VehicleLimit}";
                                    }
                                    //
                                    //Building is refinery, increase harvester limit
                                    //
                                    if (build.BuildingType == BuildingTypeEnum.Refinery)
                                    {
                                        fac.HarvestersLimit += 2 * amountToBuild;
                                        flavourText = $"Harvesters limit increased::tractor: {fac.HarvestersLimit}";
                                    }

                                    BotCore.DictionaryOfCommanders[message.Author.Id].BuildLimit -= amountToBuild;
                                    build.BuildingCount += amountToBuild;
                                    fac.FactionCredits -= build.BuildingCost*amountToBuild;
                                    fac.FactionPowerLevel -= build.BuildingPowerCost*amountToBuild;

                                    if (File.Exists(build.BuildingImagePath))
                                    {
                                        await message.Channel.SendFileAsync(build.BuildingImagePath, $"***Constuction complete: {amountToBuild}x {build.BuildingName}*** :hammer_pick:  {flavourText}{Environment.NewLine} Remaining funds: {fac.FactionCredits} Remaining power: {fac.FactionPowerLevel}");
                                    }
                                    else
                                    {
                                        await message.Channel.SendMessageAsync($"***Constuction complete: {amountToBuild}x {build.BuildingName}*** :hammer_pick:  {flavourText}{Environment.NewLine} Remaining funds: {fac.FactionCredits}");
                                    }
                                }
                                else
                                {
                                    await message.Channel.SendMessageAsync($"Low Power");
                                }
                            }
                            else
                            {
                                await message.Channel.SendMessageAsync($"Insufficient Funds");
                            }
                        }
                        else
                        {
                            await message.Channel.SendMessageAsync($"Insufficient build moves");
                        }
                    } 

                }

            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Construct Building Task {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌─────────────┐
        //│ Trains Unit │
        //└─────────────┘
        public async Task TrainUnit(IMessage message)
        {
            try
            {
                // -train X buildingname
                var begginingOfAmount = message.Content.IndexOf(' ', 0);
                var endOfAmount = message.Content.IndexOf(' ', begginingOfAmount + 1);
                int amountToTrain;
                string amountString = message.Content.ToLower().Substring(begginingOfAmount + 1, endOfAmount - begginingOfAmount - 1);
                string ObjToTrain = message.Content.ToLower().Substring(endOfAmount + 1);




                //Finds users faction
                Faction fac = BotCore.ListOfFactions.Find(x => x.FactionRoleName == BotCore.DictionaryOfCommanders[message.Author.Id].CommanderFactionName);
                Unit un = new Unit();
                //
                //Check for amount
                //
                if (int.TryParse(amountString, out amountToTrain))
                {
                    //
                    //Check if building exists
                    //
                    if (fac.FactionUnits.TryGetValue(ObjToTrain, out un))
                    {
                    //
                    //Check if user can train
                    //
                    if (BotCore.DictionaryOfCommanders[message.Author.Id].UnitLimit > 0 && amountToTrain >0 && amountToTrain <= BotCore.DictionaryOfCommanders[message.Author.Id].UnitLimit)
                    {
                        //
                        //Check if faction has enough credits
                        //
                        if (fac.FactionCredits - (un.UnitCost*amountToTrain) >= 0)
                        {
                            //
                            //Check Unit Limit
                            //
                            string flavourText = "";
                            bool SendUnit = false;
                            //
                            //Train Harvester Unit
                            //
                            if (fac.HarvestersLimit+amountToTrain > 0 && un.UnitType == UnitTypeEnum.Harvester)
                            {
                                fac.FactionCredits -= un.UnitCost*amountToTrain;
                                un.UnitCount += amountToTrain;
                                fac.HarvestersLimit -= amountToTrain;
                                BotCore.DictionaryOfCommanders[message.Author.Id].UnitLimit-= amountToTrain;
                                flavourText = $"***Harvester ready {amountToTrain}x {un.UnitName}*** | Number of active harvesters {un.UnitCount}";
                                SendUnit = true;
                            }
                            else if (fac.HarvestersLimit + amountToTrain <= 0 && un.UnitType == UnitTypeEnum.Harvester)
                            {
                                //Hit the limit
                                flavourText = $"Limit of Harvesters reached, construct additional Refineries.";
                            }
                            //
                            //Train Infantry unit
                            //
                            if (fac.InfantryLimit > 0 && un.UnitType == UnitTypeEnum.Infantry)
                            {
                                fac.FactionCredits -= un.UnitCost*amountToTrain;
                                un.UnitCount+=amountToTrain;
                                fac.InfantryLimit-=amountToTrain;
                                BotCore.DictionaryOfCommanders[message.Author.Id].UnitLimit-=amountToTrain;
                                flavourText = $"***Unit trained {amountToTrain}x {un.UnitName}*** | Number of active soldiers {un.UnitCount}";
                                SendUnit = true;
                            }
                            else if (fac.InfantryLimit <= 0 && un.UnitType == UnitTypeEnum.Infantry)
                            {
                                //Hit the limit
                                flavourText = $"Limit of Infantry reached, construct additional Barracks.";
                            }
                            //
                            //Train Vehicle unit
                            //
                            if (fac.VehicleLimit > 0 && un.UnitType == UnitTypeEnum.Vehicle)
                            {
                                fac.FactionCredits -= un.UnitCost*amountToTrain;
                                un.UnitCount+=amountToTrain;
                                fac.VehicleLimit-=amountToTrain;
                                BotCore.DictionaryOfCommanders[message.Author.Id].UnitLimit-=amountToTrain;
                                flavourText = $"***Unit complete {amountToTrain}x {un.UnitName}*** | Number of active ground vehicles {un.UnitCount}";
                                SendUnit = true;
                            }
                            else if (fac.VehicleLimit <= 0 && un.UnitType == UnitTypeEnum.Vehicle)
                            {
                                //Hit the limit
                                flavourText = $"Limit of ground vehicles reached, construct additional Factories.";
                            }
                            //
                            //Train Aircraft unit
                            //
                            if (fac.AircraftLimit > 0 && un.UnitType == UnitTypeEnum.Aircraft)
                            {
                                fac.FactionCredits -= un.UnitCost*amountToTrain;
                                un.UnitCount+=amountToTrain;
                                fac.AircraftLimit-=amountToTrain;
                                BotCore.DictionaryOfCommanders[message.Author.Id].UnitLimit-=amountToTrain;
                                flavourText = $"***Unit complete {amountToTrain}x {un.UnitName}*** | Number of active air vehicles {un.UnitCount}";
                                SendUnit = true;
                            }
                            else if (fac.VehicleLimit <= 0 && un.UnitType == UnitTypeEnum.Aircraft)
                            {
                                //Hit the limit
                                flavourText = $"Limit of air vehicles reached, construct additional Airfields.";
                            }
                            if (File.Exists(un.UnitImagePath) && SendUnit == true)
                            {
                                await message.Channel.SendFileAsync(un.UnitImagePath, $"{flavourText}{Environment.NewLine} Remaining funds: {fac.FactionCredits}");
                            }
                            else
                            {
                                await message.Channel.SendMessageAsync($"{flavourText}{Environment.NewLine} Remaining funds: {fac.FactionCredits}");
                            }

                        }
                        //Not enough money
                        else
                        {
                            await message.Channel.SendMessageAsync($"Insufficient Funds");
                        }
                    }
                    //Not enough train moves
                    else
                    {
                        await message.Channel.SendMessageAsync($"Insufficient train moves.");
                    }
                }
            }


            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Train Unit Task {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌────────────────────┐
        //│ Battle of Factions │
        //└────────────────────┘
        public async Task BattleOfFactions()
        {
            try
            {

            IMessageChannel chn = await BotCore.BotClient.GetChannelAsync(BotCore.BotSettings.BattlefieldChannel) as IMessageChannel;
            string battletext = $"------***Battle of Factions Begins***------{Environment.NewLine}";
            
            //
            //Find Total Damage
            //
            foreach(Faction fac in BotCore.ListOfFactions)
            {
                int facDamage = 0;
                battletext += $"***{fac.FactionName}***{Environment.NewLine}";
                foreach(Unit uni in fac.FactionUnits.Values)
                {
                    facDamage += uni.UnitCount* uni.UnitDmg;
                    battletext += $"{uni.UnitName} : x{uni.UnitCount} : {uni.UnitCount * uni.UnitDmg}dmg{Environment.NewLine}";
                }
                fac.TotalUnitDamage = facDamage;
                battletext += $"Total damage: :crossed_swords: {fac.TotalUnitDamage} {Environment.NewLine}";
            }
            await chn.SendMessageAsync(battletext);
            await Task.Delay(2000);
            var orderedFactionList = BotCore.ListOfFactions.OrderByDescending(x => x.TotalUnitDamage).ToList();

            //
            //Battle Top
            //
            battletext = $"{Environment.NewLine}Round 1 {orderedFactionList[0].FactionName} VS {orderedFactionList[1].FactionName}{Environment.NewLine}";
            await chn.SendMessageAsync(battletext);
            await BattleRound(orderedFactionList[0], orderedFactionList[1], chn);
            await Task.Delay(2000);
            
            //
            //Battle Low
            //
            battletext = $"{Environment.NewLine}Round 2 {orderedFactionList[2].FactionName} VS {orderedFactionList[3].FactionName}{Environment.NewLine}";
            await chn.SendMessageAsync(battletext);

            await BattleRound(orderedFactionList[2], orderedFactionList[3], chn);
            await Task.Delay(2000);

            //
            //Reorder the list again for final battle
            //
            //Find Total Damage
            //
            foreach (Faction fac in BotCore.ListOfFactions)
                {
                    int facDamage = 0;
                    foreach (Unit uni in fac.FactionUnits.Values)
                    {
                        facDamage += uni.UnitCount * uni.UnitDmg;
                    }
                    fac.TotalUnitDamage = facDamage;
                }
            orderedFactionList = BotCore.ListOfFactions.OrderByDescending(x => x.TotalUnitDamage).ToList();


            //
            //Battle Final
            //
            battletext = $"{Environment.NewLine}Round 3 {orderedFactionList[0].FactionName} VS {orderedFactionList[1].FactionName}{Environment.NewLine}";
            await chn.SendMessageAsync(battletext);
            await BattleRound(orderedFactionList[0], orderedFactionList[1], chn);

            orderedFactionList[0].NumberOfWins++;
            //Who Won
            await chn.SendMessageAsync(battletext);
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Battle Task {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }


        }
        public async Task BattleRound(Faction fac1, Faction fac2,IMessageChannel battlechannel)
        {
            var casualties = (double)fac2.TotalUnitDamage / (double)fac1.TotalUnitDamage;
            if (casualties != 1)
            {
                if (casualties != 0 && double.IsNaN(casualties) == false)
                {
                    await battlechannel.SendMessageAsync($"Winner is ***{fac1.FactionName}***.{Environment.NewLine}Casualties are {100 * Math.Round(casualties, 2)}%{Environment.NewLine}");
                    await DamageUnits(fac1, casualties);
                    await DamageUnits(fac2, 0);
                }
                else
                {
                    await battlechannel.SendMessageAsync($"Winner is ***{fac1.FactionName}***.{Environment.NewLine}Without cassualties");

                }


            }
            else if (casualties == 1)
            {
                await battlechannel.SendMessageAsync($"100% Casualties on both sides. Draw{Environment.NewLine}");
                await DamageUnits(fac1, 0);
                await DamageUnits(fac2, 0);
            }
        }
        //┌─────────────┐
        //│ Kills Units │
        //└─────────────┘
        public async Task DamageUnits(Faction fac,double dmg)
        {
            try
            {

            var tempval = 0.0;
            
            foreach(Unit unit in fac.FactionUnits.Values)
            {
                    if(unit.UnitType != UnitTypeEnum.Harvester)
                    {

                        if((double)unit.UnitCount * dmg >= 0)
                        {
                            if(dmg != 0)
                            {
                        
                            tempval = (double)unit.UnitCount;
                            tempval *= dmg;
                            unit.UnitCount -= (int)tempval;
                            //Fix supply for dead units
                            if(unit.UnitType == UnitTypeEnum.Aircraft)
                                {
                                    fac.AircraftLimit += (int)tempval;
                                }
                                if (unit.UnitType == UnitTypeEnum.Infantry)
                                {
                                    fac.InfantryLimit += (int)tempval;
                                }
                                if (unit.UnitType == UnitTypeEnum.Vehicle)
                                {
                                    fac.VehicleLimit += (int)tempval;
                                }


                            }
                            else
                            {

                                unit.UnitCount = 0;
                                //Fix supply for dead units
                                var buildlist = fac.FactionBuildings.ToList();
                                
                                //Fix supply for dead units
                                if (unit.UnitType == UnitTypeEnum.Aircraft)
                                {
                                    fac.AircraftLimit = 5 + (5*buildlist.Find(x => x.Value.BuildingType == BuildingTypeEnum.Airfield).Value.BuildingCount);
                                }
                                if (unit.UnitType == UnitTypeEnum.Infantry)
                                {
                                    fac.InfantryLimit = 20 + (20* buildlist.Find(x => x.Value.BuildingType == BuildingTypeEnum.Barracks).Value.BuildingCount);
                                }
                                if (unit.UnitType == UnitTypeEnum.Vehicle)
                                {
                                    fac.VehicleLimit = 10 + (10* buildlist.Find(x => x.Value.BuildingType == BuildingTypeEnum.Factory).Value.BuildingCount);
                                }
                            }
                                
                            }
                        }
                    }
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Damage Units {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
            await Task.Delay(1);
        }

        //┌────────────┐
        //│ Reset game │
        //└────────────┘
        public async Task RestartGame()
        {
            try
            {

            IMessageChannel chn = await BotCore.BotClient.GetChannelAsync(BotCore.BotSettings.BattlefieldChannel) as IMessageChannel;
            await chn.SendMessageAsync("Game is restarting");
            
            foreach (Faction fac in BotCore.ListOfFactions)
            {
                fac.FactionCredits = 10000;
                fac.FactionPowerLevel = 100;
                fac.HarvestersLimit = 1;
                fac.InfantryLimit = 20;
                fac.VehicleLimit = 10;
                fac.AircraftLimit = 5;
                
                foreach (Unit unit in fac.FactionUnits.Values)
                {
                    if(unit.UnitType == UnitTypeEnum.Harvester)
                    {
                    unit.UnitCount = 1;
                    }
                    else
                    {
                    unit.UnitCount = 0;
                    }
                }
            }

            await chn.SendMessageAsync("Game was restarted");
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Restart Game Task {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }

    }
}
