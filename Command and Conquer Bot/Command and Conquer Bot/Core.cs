﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Discord;
using Discord.WebSocket;
using System.Timers;

namespace Command_and_Conquer_Bot
{
    public class Core
    {
        static void Main(string[] args) => new Core().BotStart().GetAwaiter().GetResult();
        public static void DebugLog(object sender, UnhandledExceptionEventArgs args)
        {
            Exception ex = (Exception)args.ExceptionObject;
            File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in BotLoop {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
        }
        //┌───────────┐
        //│ Variables │
        //└───────────┘
        public DiscordSocketClient BotClient;           //Actual Discord client used for everything discord related.
        public IUser BotUser;
        public List<string> BotLogMessageList = new List<string>();
        public int MessageLimit = 0;

        //
        //Time Variables
        //
        public Timer BotLooperTimer = new Timer();          //Timer for a bot loop used for timed saves, setting of a game etc..
        public DateTime RandomGameDate = new DateTime();    //Date for the Random game timer
        public DateTime ChatLogDate = new DateTime();
        public DateTime DailyDate = new DateTime();
        public DateTime HarvestDate = new DateTime();
        public int HarvestDelay = 9;
        //
        //Class references
        //
        public Settings BotSettings = new Settings();      //Settings class used for storing Bot's Username and Token, can be expanded for more settigns
        public BasicTasks BotBasicTasks;                   //Basic tasks class used for loading settigns, changing game, etc...
        public FactionManagement BotFactionManagement;     //Faction related tasks
        public CrateManagement BotCrateManagement;         //Crate related tasks

        //
        //RTS
        //
        public Dictionary<ulong, Commander> DictionaryOfCommanders = new Dictionary<ulong, Commander>();
        public List<Faction> ListOfFactions = new List<Faction>();
        public List<Crate> ListOfCrates = new List<Crate>();
        public bool Battled = false;
        public bool AnnouncedBattle = false;
        //┌────────────────────┐
        //│ Bot Initialization │
        //└────────────────────┘
        public async Task BotStart()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(DebugLog);
            //Set Encoding
            Console.OutputEncoding = Encoding.Unicode;
            PrintUsingColor(ConsoleColor.Red, "[O_O]", "BOT STARTING");
            //
            //Assign class reference
            //
            BotBasicTasks = new BasicTasks(this);
            BotFactionManagement = new FactionManagement(this);
            BotCrateManagement = new CrateManagement(this);

            //Load settings from settings.json with Task
            BotSettings = await BotBasicTasks.LoadSettingsTask();
            await BotBasicTasks.LoadFactUsersTask();
            //Actual bot client
            BotClient = new DiscordSocketClient();

            //Assign async methods to events
            BotClient.MessageReceived += async (message) => { PublicMessages(message); await Task.Delay(1); };
            BotClient.MessageReceived += async (message) => { PrivateMessages(message); await Task.Delay(1); };
            BotClient.MessageReceived += async (message) => { MentionMessages(message); await Task.Delay(1); };
            BotClient.MessageReceived += async (message) => { BotMessages(message); await Task.Delay(1); };
            BotClient.MessageReceived += async (message) => { FactionMessages(message); await Task.Delay(1); };
            BotClient.MessageReceived += async (message) => { ModOnlyMessages(message); await Task.Delay(1); };
            BotClient.UserJoined += async (user) => { UserJoinedGuild(user); await Task.Delay(1); };
            BotClient.UserPresenceUpdated += async (user, presenceBefore,presenceAfter) => { UserUpdatedStatus(user, presenceBefore, presenceAfter); await Task.Delay(1);};
            //
            //Login & Connect process
            //
            PrintUsingColor(ConsoleColor.Red, "[O_O]", $"BOT LOGGING IN");
            await BotClient.LoginAsync(TokenType.Bot, BotSettings.BotLoginToken);
            PrintUsingColor(ConsoleColor.Green, "[O_O]", $"BOT LOGGED IN, CONNECTING");
            
            await BotClient.ConnectAsync();
            PrintUsingColor(ConsoleColor.Green, "[O_O]", $"BOT CONNECTED");

            await Task.Delay(500);

            //Change bots name to the one included in settings file, can be expanded for avatar change too, I prefer changing avatar in API settings
            await (await BotClient.GetCurrentUserAsync()).ModifyAsync(x => x.Username = BotSettings.BotUserName);
            BotUser = await BotClient.GetCurrentUserAsync() as IUser;
            PrintUsingColor(ConsoleColor.Green, "[O_O]", $"STARTING LOOP");
            //
            //Setup and Start Bot Loop
            //
            BotLooperTimer.Elapsed += new ElapsedEventHandler(BotLoop);
            BotLooperTimer.Interval = 1000;
            BotLooperTimer.Start();
            //Set initial dates for loop
            RandomGameDate = DateTime.Now;
            ChatLogDate = DateTime.Now;
            DailyDate = DateTime.Now;
            HarvestDate = DateTime.Now;
            HarvestDelay = BotSettings.HarvesterDelay;
            if(DictionaryOfCommanders == null) { DictionaryOfCommanders = new Dictionary<ulong, Commander>(); }
            //Set random game at start
            Game gm = await BotBasicTasks.SetRandomGameTask();
            await (await BotClient.GetCurrentUserAsync()).ModifyStatusAsync(x => x.Game = gm);
            PrintUsingColor(ConsoleColor.Green, "[O_O]", $"STARTUP COMPLETE");
            await Task.Delay(-1);
            
        }

        //┌──────────┐
        //│ Bot Loop │
        //└──────────┘
        public async void BotLoop(object sender, ElapsedEventArgs tickEvents)
        {
            try
            {
                if (BotClient.ConnectionState == ConnectionState.Connected && BotClient.LoginState == LoginState.LoggedIn) 
                {
                    //
                    //Sets random game every hour
                    //
                    
                    if (DateTime.Now - RandomGameDate > TimeSpan.FromHours(1))
                    {
                        Game gm = await BotBasicTasks.SetRandomGameTask();
                        await (await BotClient.GetCurrentUserAsync()).ModifyStatusAsync(x => x.Game = gm);
                        RandomGameDate = DateTime.Now;
                    }
                    //
                    //Saves chatlog every 15 minutes
                    //
                    if (DateTime.Now - ChatLogDate > TimeSpan.FromMinutes(15))
                    {
                        Console.Clear();
                        await BotBasicTasks.SaveDataTask();
                        ChatLogDate = DateTime.Now;
                    }
                    //
                    //Daily reset
                    //
                    if (DailyDate.Day != DateTime.Now.Day)
                    {
                        DailyDate = DateTime.Now;

                    }
                    //
                    //Harvester and build reset
                    //
                    if (DateTime.Now - HarvestDate > TimeSpan.FromHours(HarvestDelay))
                    {
                        HarvestDate = DateTime.Now;
                        await BotBasicTasks.DailyResetTask();

                    }
                    //
                    //Crate sender
                    //
                    foreach (Crate cr in ListOfCrates)
                    {
                        BotCrateManagement.CrateSender(cr);
                    }
                    //
                    //Weekly battle
                    //
                    if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday && Battled == false)
                    {
                        if(DateTime.Now.Hour == 11 && AnnouncedBattle == false && DateTime.Now.Minute == 0)
                        {
                            AnnouncedBattle = true;
                            IMessageChannel chn = await BotClient.GetChannelAsync(BotSettings.BattlefieldChannel) as IMessageChannel;
                            await chn.SendMessageAsync("***Battle of Factions will commence in 1 hour.***");
                        }
                        //High noon battle
                        if(DateTime.Now.Hour == 12 && DateTime.Now.Minute == 0)
                        {
                            //BATTLE
                        Battled = true;
                        await BotFactionManagement.BattleOfFactions();
                        }
                    }
                    if (DateTime.Now.DayOfWeek == DayOfWeek.Monday && Battled == true)
                    {
                        Battled = false;
                        AnnouncedBattle = false;
                    }
                }
            }
            catch(Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in BotLoop {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌───────────────────────────────────────────────────────┐
        //│ Reading and responding to public messages in channels │
        //└───────────────────────────────────────────────────────┘
        public async void PublicMessages(IMessage message)
        {
            //
            //Check if Channel isn't private and if message is not from bot
            //
            try
            {

                if (!message.Author.IsBot && message.Channel is IDMChannel == false && MessageLimit < 6)
                {
                MessageLimit++;
                    //
                    //Check if user has a Commander object
                    //
                if (!DictionaryOfCommanders.ContainsKey(message.Author.Id))
                {
                    DictionaryOfCommanders.Add(message.Author.Id, new Commander() {CommanderName  = message.Author.Username,CommanderId = message.Author.Id });
                }

                PrintUsingColor(ConsoleColor.Yellow, $"{message.Channel}", $"{message.Author.Username}: {message.Content}");
                //
                //Sends Help.txt to the user
                //
                if (message.Content.ToLower() == "-help")
                {
                    if (File.Exists("Data\\Commands.txt"))
                    {
                        await message.DeleteAsync();
                        string helpString = File.ReadAllText("Data\\Commands.txt", Encoding.GetEncoding("windows-1250"));
                        IGuildUser usr = message.Author as IGuildUser;
                        IDMChannel chn = await usr.CreateDMChannelAsync();
                        await chn.SendMessageAsync(helpString);
                        
                    }
                }
                //
                //Collecting of random crates
                //
                if (message.Content.ToLower() == "-collect")
                {
                        await message.DeleteAsync();
                        if(DictionaryOfCommanders[message.Author.Id].HasJoinedFaction == true)
                        {

                        bool foundItem = false;
                        //
                        //Checks if Item is active and the channel message was sent is the same where the item is
                        //
                        foreach (Crate cr in ListOfCrates)
                        {
                            if (cr.CrateIsActive == true && message.Channel.Id == cr.CrateChannelHasBeenSent)
                            {
                                foundItem = true;
                                await BotCrateManagement.CollectCrateTask(cr);
                                Faction fac = ListOfFactions.Find(x => x.FactionRoleName == DictionaryOfCommanders[message.Author.Id].CommanderFactionName);
                                fac.FactionCredits += cr.CrateCredits;
                                await message.Channel.SendMessageAsync($"***Commander {message.Author.Username}*** has collected {cr.CrateCredits} credits for {fac.FactionName}");
                                break;
                            }
                            else
                            {
                                foundItem = false;
                            }
                        }
                        if (!foundItem)
                        {
                            await message.Channel.SendMessageAsync($"There are no collectables ***Commander {message.Author.Username}***");
                        }
                        }
                        //
                        //User didnt join faction
                        //
                        else
                        {
                            IGuildUser usr = message.Author as IGuildUser;
                            IDMChannel chn = await usr.CreateDMChannelAsync();
                            await chn.SendMessageAsync($"In order to collect  crates you have to join a faction ***Commander {message.Author.Username}***");
                        }
                }

                    //
                    // Faction statistics
                    //
                    if (message.Content.ToLower() == "-factions")
                    {
                        await BotFactionManagement.TopFactions(message);
                    }

                    //
                    //Joins user to a faction
                    //
                    if (message.Content.ToLower().StartsWith("-join "))
                    {
                        await message.DeleteAsync();
                        await BotFactionManagement.JoinFactionTask(message);
                    }
                    //
                    //Leaves user to a faction
                    //
                    if (message.Content.ToLower() == "-leave")
                    {
                        await message.DeleteAsync();
                        await BotFactionManagement.LeaveFactionTask(message);
                    }
                    //
                    //Show remaining moves
                    //
                    if (message.Content.ToLower() == "-moves")
                    {
                        
                        await message.DeleteAsync();
                        await message.Channel.SendMessageAsync($"***{message.Author.Username}*** remaining moves Build: {DictionaryOfCommanders[message.Author.Id].BuildLimit} | Train: {DictionaryOfCommanders[message.Author.Id].UnitLimit}");

                    }
                    //
                    //Show till harvester reset
                    //
                    if (message.Content.ToLower() == "-time")
                    {
                        TimeSpan ts = ((DateTime.Now - HarvestDate) - TimeSpan.FromHours(HarvestDelay)).Negate();
                        
                        await message.Channel.SendMessageAsync($"Time to next harvestar batch and build refresh is: {ts.Hours}:{ts.Minutes}:{ts. Seconds}");
                    }
                    MessageLimit--;
            }
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Public Messages {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌────────────────────────────────────────────┐
        //│ Reading and responding to private messages │
        //└────────────────────────────────────────────┘
        public async void PrivateMessages(IMessage message)
        {
            //
            //Check if Channel is private and if message is not from bot
            //
            try
            {

            if (!message.Author.IsBot && message.Channel is IDMChannel == true && MessageLimit < 6)
            {
                    MessageLimit++;
                    //
                    //Sends Help.txt to the user
                    //
                    if (message.Content.ToLower() == "-help")
                    {
                        if (File.Exists("Data\\Commands.txt"))
                        {
                            string helpString = File.ReadAllText("Data\\Commands.txt", Encoding.GetEncoding("windows-1250"));
                            IGuildUser usr = message.Author as IGuildUser;
                            IDMChannel chn = message.Channel as IDMChannel;
                            await chn.SendMessageAsync(helpString);

                        }
                    }
                    /*
                    PrintUsingColor(ConsoleColor.Magenta, $"{message.Channel}", $"{message.Author.Username}: {message.Content}");
                    if (message.Content == "ping")
                    {
                        await message.Channel.SendMessageAsync("PM pong");
                    }
                    */
                    MessageLimit--;
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Private Messages {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌────────────────────────────────────┐
        //│ Reading and responding to mentions │
        //└────────────────────────────────────┘
        public async void MentionMessages(IMessage message)
        {
            //
            //Check if Channel isn't private and if message is not from bot
            //
            try
            {

            if (!message.Author.IsBot && message.Channel is IDMChannel == false && message.MentionedUsers.Count > 0 && MessageLimit < 6) 
            {
                PrintUsingColor(ConsoleColor.DarkMagenta, $"{message.Channel}", $"{message.Author.Username}: {message.Content}");
                    MessageLimit++;
                    //
                    //Sends PM to all mentioned users
                    //
                    /*
                    foreach (IGuildUser usr in message.MentionedUsers)
                    {
                        if(usr.Id != BotUser.Id)
                        {
                        IDMChannel chn = await usr.CreateDMChannelAsync();
                        await chn.SendMessageAsync("Mentioned");
                        }
                    }*/
                    MessageLimit--;
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Mention messages {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌──────────────────────┐
        //│ Messages sent by bot │
        //└──────────────────────┘
        public async void BotMessages(IMessage message)
        {
            //
            //Check if message is from bot
            //
            try
            {

            if (message.Author.Id == BotUser.Id && message.Channel is IDMChannel == false)
            {
                    //
                    //Assigning ids to crates
                    //
                    if(message.Attachments.Count > 0)
                    {

                    foreach(IAttachment att in message.Attachments)
                        {
                            Crate crat = ListOfCrates.Find(x => x.CrateImagePath.ToLower().Contains(att.Filename.ToLower()));
                            if(crat != null)
                            {
                                crat.CrateMessageID = message.Id;
                                crat.CrateHasMessageId = true;
                                crat.CrateChannelHasBeenSent = message.Channel.Id;
                                PrintUsingColor(ConsoleColor.DarkRed, "Crate", $"Found Crate id {message.Id} credits {crat.CrateCredits} channel {message.Channel}");
                                break;
                            }
                        }
                    }

            }
            await Task.Delay(1);
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Bot Messages {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌───────────────────────────────────┐
        //│ Messages for Faction channel only │
        //└───────────────────────────────────┘
        public async void FactionMessages(IMessage message)
        {
            try
            {

            foreach(Faction fc in ListOfFactions)
            {
                    if (fc.FactionChannelId == message.Channel.Id)
                    {
                        PrintUsingColor(ConsoleColor.DarkCyan, $"{message.Channel}", $"{message.Author.Username}: {message.Content}");

                        //
                        //Displays faction status
                        //
                        if (message.Content.ToLower() == "-state")
                        {
                            await message.DeleteAsync();
                            string stateString = "";
                            stateString += $"***{fc.FactionName}***{Environment.NewLine}";
                            stateString += $":busts_in_silhouette: Commanders: {fc.FactionMembers}{Environment.NewLine}";
                            stateString += $":moneybag: Funds: {fc.FactionCredits}{Environment.NewLine}";
                            stateString += $":zap: Power: {fc.FactionPowerLevel}{Environment.NewLine}";
                            stateString += $":gun: Remaining Infantry supply {fc.InfantryLimit}{Environment.NewLine}";
                            stateString += $":airplane: Remaining Aircraft supply {fc.AircraftLimit}{Environment.NewLine}";
                            stateString += $":red_car: Remaining Ground vehicle supply {fc.VehicleLimit}{Environment.NewLine}";
                            stateString += $":tractor: Remaining Harvesters supply {fc.HarvestersLimit}{Environment.NewLine}";
                            //Show units
                            await message.Channel.SendMessageAsync(stateString);
                        }
                        //
                        //Build building
                        //
                        if (message.Content.ToLower().StartsWith("-build ") || message.Content.ToLower().StartsWith("-construct "))
                        {
                            await message.DeleteAsync();
                            await BotFactionManagement.ConstructBuilding(message);
                        }
                        //
                        //Builds Unit
                        //
                        if (message.Content.ToLower().StartsWith("-train "))
                        {
                            await message.DeleteAsync();
                            await BotFactionManagement.TrainUnit(message);
                        }
                        //
                        //Show ordered buildings
                        //
                        if (message.Content.ToLower() == "-buildings")
                        {
                            await message.DeleteAsync();
                            string buildingsInfo = "";
                            var buildList = fc.FactionBuildings.OrderBy(x => x.Value.BuildingName).ToList();
                            foreach(KeyValuePair<string,Building> kp in buildList)
                            {
                                buildingsInfo += $"***{kp.Value.BuildingName}*** : Count: **{kp.Value.BuildingCount}** : :moneybag: Cost: **{kp.Value.BuildingCost}** : :zap: Power required: **{kp.Value.BuildingPowerCost}** Building type **{kp.Value.BuildingType}**{Environment.NewLine}";
                            }
                            await message.Channel.SendMessageAsync(buildingsInfo);
                        }
                        //
                        //Show ordered Units
                        //
                        if (message.Content.ToLower() == "-units")
                        {
                            await message.DeleteAsync();
                            string unitsInfo = "";
                            int TotalDmg = 0;
                            var unitList = fc.FactionUnits.OrderBy(x => x.Value.UnitName).ToList();
                            foreach (KeyValuePair<string, Unit> kp in unitList)
                            {
                                unitsInfo += $"***{kp.Value.UnitName}*** : Count: **{kp.Value.UnitCount}** : :dagger: Dmg/unit **{kp.Value.UnitDmg}** : :moneybag: Cost: **{kp.Value.UnitCost}** Unit Type: **{kp.Value.UnitType}**{Environment.NewLine}";
                                TotalDmg += kp.Value.UnitCount * kp.Value.UnitDmg;
                            }
                            unitsInfo += $"Total dmg of units :crossed_swords: {TotalDmg}";
                            await message.Channel.SendMessageAsync(unitsInfo);
                        }
                        
                    }
            }
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Faction only messages {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌────────────────────────┐
        //│ Messages for Mods only │
        //└────────────────────────┘
        public async void ModOnlyMessages(IMessage message)
        {
            try
            {

            if (!message.Author.IsBot && message.Channel is IDMChannel == false)
            {
                IGuildUser us = message.Author as IGuildUser;
                var modrole = us.Roles.ToList().Find(x => x.Id == BotSettings.ModRoleId);
                //Mod only commands
                if (modrole != null)
                {
                    //
                    //Starts battle
                    //
                    if (message.Content.ToLower() == "-battle")
                    {
                        await message.DeleteAsync();
                        IMessageChannel chn = await BotClient.GetChannelAsync(BotSettings.BattlefieldChannel) as IMessageChannel;
                        await chn.SendMessageAsync("***Blitz Battle of Factions will commence in 1 hour.***");
                        //await Task.Delay(TimeSpan.FromMinutes(60));
                        await BotFactionManagement.BattleOfFactions();
                    }
                    //
                    //Deletes messages
                    //
                    if (message.Content.ToLower().StartsWith("-clear "))
                    {
                        await message.DeleteAsync();
                        string msgsgamount = message.Content.ToLower().Substring(message.Content.IndexOf(' ') + 1);
                        int msgToDeleteAmount;
                        if(int.TryParse(msgsgamount, out msgToDeleteAmount))
                        {
                            if(msgToDeleteAmount < 21)
                            {
                                var messgs = message.Channel.GetMessagesAsync(msgToDeleteAmount);
                                foreach(IMessage mes in messgs.Result)
                                {
                                    await mes.DeleteAsync();
                                }
                            }
                        }
                    }
                    //
                    //Harvest
                    //
                    //
                    if (message.Content.ToLower() == "-harvest")
                    {
                        await BotBasicTasks.DailyResetTask();
                    }

                    //Reset the game
                    //
                    if (message.Content.ToLower() == "-restartthegamecarefulldontusewhendrunk")
                    {
                        await message.DeleteAsync();
                        await BotFactionManagement.RestartGame();
                    }

                    //
                    //Add credits to faction
                    //
                    if (message.Content.ToLower().StartsWith("-addcredits "))
                    {
                            await message.DeleteAsync();
                            var begginingOfAmount = message.Content.IndexOf(' ', 0);
                            var endOfAmount = message.Content.IndexOf(' ', begginingOfAmount + 1);
                            int amountToAdd;
                            string amount = message.Content.ToLower().Substring(begginingOfAmount + 1, endOfAmount - begginingOfAmount - 1);
                            string parseFac = message.Content.ToLower().Substring(endOfAmount + 1);
                            if(int.TryParse(amount, out amountToAdd))
                            {
                                Faction fac = ListOfFactions.Find(x => x.FactionRoleName.ToLower() == parseFac.ToLower());
                                if(fac != null)
                                {

                                fac.FactionCredits += amountToAdd;
                                await message.Channel.SendMessageAsync($"**{amountToAdd}** has been gifted to **{fac.FactionName}** by ***{message.Author.Username}***");
                                }

                            }
                    }
                    }
            }
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Mod Messages {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌───────────────────┐
        //│ User Joined Guild │
        //└───────────────────┘
        public async void UserJoinedGuild(IGuildUser user)
        {
            try
            {

            IMessageChannel welcomeChn = await user.Guild.GetChannelAsync(BotSettings.WelcomeChannelId) as IMessageChannel;
            await welcomeChn.SendMessageAsync($"***Establishing battlefield controll for Commander {user.Mention}, standby***");
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in User Joined Server {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌────────────────────────────────┐
        //│ User Presence in Guild Updated │
        //└────────────────────────────────┘
        public async void UserUpdatedStatus(IGuildUser usr, IPresence usrBefore, IPresence usrAfter)
        {
            //
            //User went online
            //
            try
            {

                if (usrAfter.Status == UserStatus.Online)
                {
                    //If user doenst have created Commander, create one
                    Commander command;
                    if (!DictionaryOfCommanders.TryGetValue(usr.Id, out command))
                    {
                        command = new Commander()
                        {
                            CommanderName = usr.Username,
                            CommanderId = usr.Id,
                        };
                        DictionaryOfCommanders.Add(usr.Id, command);
                    }
                    if (DictionaryOfCommanders[usr.Id].RecievedWelcomeMessage == false)
                    {
                        if (File.Exists("Data\\Welcome.txt"))
                        {
                            string helpString = File.ReadAllText("Data\\Welcome.txt", Encoding.GetEncoding("windows-1250"));
                            IDMChannel chn = await usr.CreateDMChannelAsync();
                            await chn.SendMessageAsync(helpString);
                            DictionaryOfCommanders[usr.Id].RecievedWelcomeMessage = true;
                        }
                    }
                }

            //
            //User went offline
            //
            if (usrAfter.Status == UserStatus.Offline)
            {
                //Update last time online
                Commander command;
                if (DictionaryOfCommanders.TryGetValue(usr.Id, out command))
                {
                    command.LastDateOnline = DateTime.Now;
                }
                else
                {
                    command = new Commander()
                    {
                        CommanderName = usr.Username,
                        CommanderId = usr.Id,
                    };
                    DictionaryOfCommanders.Add(usr.Id, command);
                }
            }
            await Task.Delay(1);

            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in User pressence update {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
        //┌────────────────────────────────────────────────────────────────────┐
        //│ Write to console with colored prefix, add it to a list for logging │
        //└────────────────────────────────────────────────────────────────────┘
        public void PrintUsingColor(ConsoleColor prefixColor, string prefix, string message)
        {
            ConsoleColor origColor = ConsoleColor.White;   //Save the original Color
            Console.ForegroundColor = ConsoleColor.Cyan;        //Set Color of Date to cyan
            Console.Write($"{DateTime.Now.ToLongTimeString()}");//Start the message with a Time
            Console.ForegroundColor = prefixColor;              //Set Color of the prefix to parsed parameter
            Console.Write($" │ {prefix} │ ");                   //Add a prefix to the message
            Console.ForegroundColor = origColor;                //Reset color to original
            Console.Write($"{message}{Environment.NewLine}");
           BotLogMessageList.Add($"{DateTime.Now.Day}/{DateTime.Now.Month}/{DateTime.Now.Year} {DateTime.Now.ToLongTimeString()} │ {prefix} │ {message}"); //Add message to list for saving
            
        }
    }
}
