﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_and_Conquer_Bot
{
    public class Commander
    {
        public string CommanderName { get; set; }
        public ulong CommanderId { get; set; }
        public bool RecievedWelcomeMessage { get; set;}
        public DateTime LastDateOnline { get; set; }
        public DateTime JoinedAtDate { get; set; }


        public string CommanderFactionName { get; set; }
        public bool HasJoinedFaction { get; set; }

        public int BuildLimit { get; set; }
        public int UnitLimit { get; set; }

        public Commander()
        {
            CommanderName = "Default Commander Name";
            CommanderId = 1231123123123;
            CommanderFactionName = "Default Commander Faction Name";
            HasJoinedFaction = false;
            RecievedWelcomeMessage = false;
            JoinedAtDate = DateTime.Now;
            BuildLimit = 10;
            UnitLimit = 20;
        }

    }
}
