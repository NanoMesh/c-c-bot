﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_and_Conquer_Bot
{
    public class Crate
    {
        public int CrateCredits { get; set; }
        public int CrateCreditsThresholdLow { get; set; }
        public int CrateCreditsThresholdHigh { get; set; }
        public bool CrateHasMessageId { get; set; }
        public ulong CrateMessageID { get; set; }
        public bool CrateIsActive { get; set; }
        public DateTime CrateLastTimeSent { get; set; }
        public string CrateImagePath { get; set; }
        public int CrateDelay { get; set; }
        public int CrateDelayThresholdLow { get; set; }
        public int CrateDelayThresholdHigh { get; set; }
        public ulong CrateChannelHasBeenSent { get; set; }

        public Crate()
        {
            CrateHasMessageId = false;
            CrateIsActive = false;
            CrateLastTimeSent = DateTime.Now;
            CrateImagePath = "Default Image Path";
        }


    }
}
