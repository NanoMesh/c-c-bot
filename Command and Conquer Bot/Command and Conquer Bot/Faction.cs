﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_and_Conquer_Bot
{
    public class Faction
    {
        public string FactionName { get; set; }
        public string FactionRoleName { get; set; }
        public ulong FactionChannelId { get; set; }
        public int FactionCredits { get; set; }
        public int FactionPowerLevel { get; set; }
        public int FactionMembers { get; set; }
        public int HarvestersLimit { get; set; }
        public int InfantryLimit { get; set; }
        public int VehicleLimit { get; set; }
        public int AircraftLimit { get; set; }
        public int TotalUnitDamage { get; set; }
        public int NumberOfWins { get; set; }
        public Dictionary<string,Building> FactionBuildings { get; set; }
        public Dictionary<string,Unit> FactionUnits { get; set; }


        public Faction()
        {
            FactionName = "Default Faction Name";
            FactionRoleName = "Default Faction Role Name";
            FactionCredits = 10000;
            FactionMembers = 0;
            FactionPowerLevel = 100;
            FactionChannelId = 123123123;
            HarvestersLimit = 1;
            InfantryLimit = 20;
            VehicleLimit = 10;
            AircraftLimit = 5;
            NumberOfWins = 0;
            FactionBuildings = new Dictionary<string, Building>();
            FactionUnits = new Dictionary<string, Unit>();
        }
    }
}
