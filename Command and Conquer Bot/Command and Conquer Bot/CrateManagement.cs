﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using System.IO;

namespace Command_and_Conquer_Bot
{
    public class CrateManagement
    {
        public Core BotCore;

        public CrateManagement(Core cr)
        {
            BotCore = cr;
        }

        //┌───────────────────────────┐
        //│ Sends crate if not active │
        //└───────────────────────────┘
        public async void CrateSender(Crate crate)
        {
            //Sends crate
            if (DateTime.Now - crate.CrateLastTimeSent > TimeSpan.FromMinutes(crate.CrateDelay) && crate.CrateIsActive == false)
            {
                await SendCrateTask(crate);
                

            }
            //Destroys crate if not collected
            if (DateTime.Now - crate.CrateLastTimeSent > TimeSpan.FromMinutes(crate.CrateDelay + 5) && crate.CrateIsActive == true)
            {
                BotCore.PrintUsingColor(ConsoleColor.DarkRed, "Crate", $"Crate not collected, cleaning up.");
                await CollectCrateTask(crate);
            }
        }
        //┌─────────────────┐
        //│ Send crate task │
        //└─────────────────┘
        public async Task SendCrateTask(Crate crate)
        {
            try
            {

            Random rand = new Random();
            crate.CrateChannelHasBeenSent = BotCore.BotSettings.RandomCrateChannels[rand.Next(BotCore.BotSettings.RandomCrateChannels.Count)];
            IReadOnlyCollection<IGuild> guildList = await BotCore.BotClient.GetGuildsAsync();
            foreach (IGuild srv in guildList)
            {
                IMessageChannel chn = await srv.GetChannelAsync(crate.CrateChannelHasBeenSent) as IMessageChannel;
                if (chn != null)
                {
                    await Task.Delay(100);
                    crate.CrateHasMessageId = false;
                    crate.CrateIsActive = true;
                    crate.CrateCredits = rand.Next(crate.CrateCreditsThresholdLow, crate.CrateCreditsThresholdHigh);
                    await chn.SendFileAsync(crate.CrateImagePath);
                }
            }
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Send Crate Task {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }

        }

        public async Task CollectCrateTask(Crate crate)
        {
            try
            {

            IReadOnlyCollection<IGuild> guildList = await BotCore.BotClient.GetGuildsAsync();
            foreach (IGuild srv in guildList)
            {
                IMessageChannel chn = await srv.GetChannelAsync(crate.CrateChannelHasBeenSent) as IMessageChannel;
                if (chn != null)
                {
                    //
                    //Try delete message only when found
                    //
                    var msgs = await chn.GetMessagesAsync(10);
                    foreach (IMessage ms in msgs)
                    {
                        if(ms.Id == crate.CrateMessageID)
                        {
                            await ms.DeleteAsync();
                        }

                    }
                    crate.CrateIsActive = false;
                    crate.CrateLastTimeSent = DateTime.Now;
                    Random rand = new Random();
                    crate.CrateDelay = rand.Next(crate.CrateDelayThresholdLow, crate.CrateDelayThresholdHigh);
                    BotCore.PrintUsingColor(ConsoleColor.DarkRed, "Crate", $"Collected crate with {crate.CrateCredits} credits. Next delay {crate.CrateDelay}.");
                }
            }
            }
            catch (Exception ex)
            {
                File.AppendAllText("Debug.txt", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} Error in Collect Crate Task {Environment.NewLine}{ex.ToString()}{Environment.NewLine}");
            }
        }
    }
}
