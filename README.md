**Command & Conquer Discord.Net 1.0 Bot**
-----------------------------

This is a simplistic bot made for [Discord.Net](https://github.com/RogueException/Discord.Net) version 1.0.
Its made as a more detailed example on how to work with the library.
Included features are
 - Responding to public messages in channel
 - Responding to PM messages sent directly to the bot
 - Dealing with user mentioning
 - Greeting user upon joining a Guild(Server) 
 - Setting a Random game
 - Saving chat log to a file

----------


Word of advice though, it is my pig-level code so do not expect perfectly functional or proper way to do things. Its made my way that works for me. Do not be affraid to just take a look on how to do some things and then do them completly differently.
Also don't forget to update packages in NuGet manager and from [https://www.myget.org/F/discord-net/api/v3/index.json](https://www.myget.org/F/discord-net/api/v3/index.json)

----------


If you got questions about the library itself i suggest hopping on here [Discord-API Server](https://discord.gg/discord-api), there is a channel dedicated to Discord.Net.

Otherwise if you got a general C# related question i suggest joining [C#](https://discord.gg/bSMMNpT) server.

If you got any particuliar questions for me than try PM me on Discord. You can find me either on the API server or on C# server @Nanomesh.

Hopefully you'll find this bot helpfull in your future endeavours.